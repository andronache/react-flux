import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';

class PostStore extends EventEmitter {
  constructor(){
    super();
    this.addPost = this.addPost.bind(this);
    this.deletePost = this.deletePost.bind(this);
    this.editPost = this.editPost.bind(this);
    this.getAll = this.getAll.bind(this);
    this.posts = [
      {
        id: 'x0x0x0',
        title: 'First Title',
        content: 'Lorem ipsum dolor sit amet',
        date: '18.12.2017'
      },
      {
        id: 'y2y2y2',
        title: 'Second Title',
        content: 'Lorem ipsum dolor sit amet',
        date: '18.12.2017'
      }
    ]
  };
  addPost(info) {
    let posts = this.posts;
    //date
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();
    let fullDate = dd + "." + mm + "." + yyyy;
    //random ID
    let postID = "";
    let pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 5; i++)
      postID += pool.charAt(Math.floor(Math.random() * pool.length));

    let post = {
      id: postID,
      title: info.title,
      content: info.content,
      date: fullDate
    };
    posts.push(post);
    this.posts = posts;
    this.emit("change");
  };
  deletePost(postID) {
    let posts = this.posts;
    posts.map((post, i) => {
      if(post.id === postID){
        posts.splice(i, 1);
      }
    })
    this.posts = posts;
    this.emit("change");
  };
  editPost(info) {
    let posts = this.posts;
    posts.map((post, i) => {
      if(post.id === info.id){
        posts[i].title = info.title;
        posts[i].content = info.content;
      }
    })
    this.posts = posts;
    this.emit("change");
  };
  getAll(){
    return this.posts;
  };
  handleActions(action){
    switch(action.type){
      case "EDIT_POST": {
        this.editPost(action.info);
        break;
      }
      case "DELETE_POST": {
        this.deletePost(action.id);
        break;     }
      case "ADD_POST": {
        this.addPost(action.info);
        break;
      }
      case "GET_ALL": {
        this.getAll();
        break;
      }
      default: {
        alert("wtf? u lil' hacker!");
      }
    }
  }
}
const postStore = new PostStore;
dispatcher.register(postStore.handleActions.bind(postStore));
window.postStore = postStore;
// window.dispatcher = dispatcher;
export default postStore;
