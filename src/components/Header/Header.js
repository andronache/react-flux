import React from 'react';
import {
  NavLink,
  Link,
  withRouter
} from 'react-router-dom';
import Modal from 'react-modal';

const linkList = [
  {
    name: 'Home',
    link: '/'
  },
  {
    name: '+Add',
    link: '/add'
  },
  {
    name: '?About',
    link: "/about"
  }
];
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

class Header extends React.Component {
  constructor() {
    super();
    this.state = {
      loginIsOpen: false
    }
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.confirm = this.confirm.bind(this);
  };
  openModal() {
    this.setState({loginIsOpen: true});
  };
  closeModal() {
    this.setState({loginIsOpen: false});
  };
  confirm() {
    this.props.history.push("/");
  };
  render() {
    return (
      <nav className='col-md-9 navbar navbar-default menu'>
        <div className='container'>
          <div className='navbar-header'>
            <ul className='nav navbar-nav'>
              {linkList.map((link, i)=>{
                return (
                  <li key={i}><NavLink exact activeClassName="activeMenu" to={linkList[i].link}> {linkList[i].name} </NavLink></li>
                );
              })}
            </ul>
          </div>
          <button className="btn btn-primary btn-sm menuItem"
            onClick={this.openModal}>Login</button>
          <Modal
            isOpen={this.state.loginIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            style={customStyles}
            contentLabel=""
          >
            <div className="col-md-12 modal-container">
              <div className="modal-header">
                <h2 className="modal-title" id="exampleModalLabel"ref={subtitle => this.subtitle = subtitle}>Login</h2>
              </div>
              <div className="modal-body">
                <form>
                  <div className="form-group">
                    <label className="form-control-label">Email:
                      <input type="text" className="form-control" id="recipient-name"></input>
                    </label>
                  </div>
                  <div className="form-group">
                    <label className="form-control-label">Password:
                      <input type="password" className="form-control" id="recipient-name"></input>
                    </label>
                  </div>
                  <Link to={"/register"}>Don't have an account?</Link>
                  <button className="btn modalBtn" type="button" onClick={() => {this.confirm}}>Access</button>
                </form>
              </div>
              {/* <div className="modalBtnDiv">
                <Link to={"/"} >
                  <div className="modalBtn">Login</div>
                </Link>
              </div> */}
            </div>
          </Modal>
        </div>
      </nav>
    );
  }
}
export default withRouter(Header);
