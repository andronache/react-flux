import React from "react";
import {
  Link
} from 'react-router-dom';
import * as PostActions from '../../actions/PostActions';
import { Header } from "../";

const Post = (props) => {
  const deletePost = () => {
    PostActions.deletePost(props.location.state.post.id);
    props.history.push("/");
  }
  return (
    <div className="container">
      <Header />
      <div className="col-md-9 wrapper">
        <div className="col-md-11 postPreview">
          <Link to={"/post/" + props.location.state.post.id} className="postTitle">{props.location.state.post.title}</Link>
          <span className="postDate">{props.location.state.post.date}</span>
          <button type="button" onClick={deletePost} className="btn btn-danger btn-sm float-right editDeleteBtn">Delete</button>
          <Link className="postTitle" to={{
            pathname: '/edit/',
            state: {
              post: props.location.state.post,
            }
          }}><button type="button" className="btn btn-outline-secondary btn-sm float-right editDeleteBtn">Edit</button></Link>
          <hr/>
          <span>{props.location.state.post.content}</span>
          <hr/>
        </div>
      </div>
    </div>
  );
}

/* PROPS.LOCATION.STATE.POST BY DEFAULT */
export default Post;
