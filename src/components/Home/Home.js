import React from "react";
import postStore from '../../stores/PostStore';
import * as PostActions from '../../actions/PostActions';
import { PostPrev } from "../";
import { Header } from "../";

export default class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      posts : PostActions.getAll()
    };
    console.log(this.state);
  };
  componentWillMount() {
    postStore.on("change", () => {
      this.setState({
        posts : PostActions.getAll()
      });
    });
  };
  render() {
    return (
      <div className="container">
        <Header />
        <div className="col-md-9 wrapper">
          <div className="row">
            {
              this.state.posts.map((post, i) => {
                return (
                  <PostPrev
                    key={post.id}
                    post={post} />
                );
              })
            }
          </div>
        </div>
      </div>
    );
  }
}
