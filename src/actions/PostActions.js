import dispatcher from '../dispatcher';

export function addPost(infoo) {
  dispatcher.dispatch({
    type: "ADD_POST",
    info: {
      title: infoo.title,
      content: infoo.content
    }
  });
}
export function editPost(post) {
  dispatcher.dispatch({
    type: "EDIT_POST",
    info: {
      id: post.id,
      title: post.title,
      content: post.content
    }
  });
}
export function deletePost(id) {
  dispatcher.dispatch({
    type: "DELETE_POST",
    id
  });
}
export function getAll() {
  dispatcher.dispatch({
    type: "GET_ALL"
  });
}
