import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import {
  Home,
  About,
  Add,
  Post,
  Edit
} from './components';

export default class App extends React.Component {
  render() {
    return(
      <Router>
        <Switch>
          <Route exact path={"/"} component={Home}/>
          <Route path={"/about"} component={About} />
          <Route path={"/post/:id"} component={Post} />
          <Route path={"/edit"} component={Edit} />
          <Route path={"/add"} component={Add} />
        </Switch>
      </Router>
    );
  }
}
